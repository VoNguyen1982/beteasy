using dotnet_code_challenge.Models;
using dotnet_code_challenge.Services;
using System;
using System.IO;
using Xunit;

namespace dotnet_code_challenge.Test
{
    public class ServiceTest
    {
        string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        [Fact]
        public void OrderByTest()
        {
            IViewModel model = new ViewModel();
            model.Rows = new System.Collections.Generic.List<Row>();
            model.Rows.Add(new Row() { Name = "C", Price = 1.2m });
            model.Rows.Add(new Row() { Name = "B", Price = 1.1m });
            model.Rows.Add(new Row() { Name = "A", Price = 1.0m });
            var result = model.OrderByAsc();
            Assert.Equal("A", result[0].Name);
            Assert.Equal("B", result[1].Name);
            Assert.Equal("C", result[2].Name);
            Assert.Equal(1.0m, result[0].Price);
            Assert.Equal(1.1m, result[1].Price);
            Assert.Equal(1.2m, result[2].Price);
        }

        [Fact]
        public void ReadFileTest()
        {
            var path = string.Format("{0}\\{1}", directory, "FeedData\\Wolferhampton_Race1.json");
            IFileService service = new FileService(path);
            string result = service.ReadFile();
            Assert.NotEmpty(result);
        }

        [Fact]
        public void JsonMappingTest()
        {
            var path = string.Format("{0}\\{1}", directory, "FeedData\\Wolferhampton_Race1.json");
            IFileService service = new FileService(path);
            string result = service.ReadFile();

            IMappingService mService = new JsonMappingService();
            var model = mService.MappingToModel(result);

            Assert.Equal(2, model.Rows.Count);
        }

        [Fact]
        public void XmlMappingTest()
        {
            var path = string.Format("{0}\\{1}", directory, "FeedData\\Caulfield_Race1.xml");
            IFileService service = new FileService(path);
            string result = service.ReadFile();

            IMappingService mService = new XmlMappingService();
            var model = mService.MappingToModel(result);

            Assert.Equal(2, model.Rows.Count);
        }

        [Fact]
        public void JsonServiceTest()
        {
            IMappingFactoryService service = new MappingFactoryService("a.jSon");
            Assert.IsType<JsonMappingService>(service.GetService());
        }

        [Fact]
        public void XmlServiceTest()
        {
            IMappingFactoryService service = new MappingFactoryService("a.xMl");
            Assert.IsType<XmlMappingService>(service.GetService());
        }
    }
}
