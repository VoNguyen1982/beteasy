Dear all,

There are a number of assumption I made when implementing the application.

1. I converted Json and Xml to objects by using some online tools (http://json2csharp.com/ and https://xmltocsharp.azurewebsites.net/) I hope it converted correctly. 
2. I see the 2 objects are totally different structure. Also horse name and price are in different places in object so I had to make a loop to extract the name and price. 
(I hope I don't make it over complicated)).The way I using the loop may not perfect solution but it saves me some time to complete the application.
3. I ran out of time to implement LoggingService
4. I print result in console rather than create API or webpage
5. The URL does not work so I was using files FeedData.
6. I wonder you guys want to print out and sort the results from 2 data feeds together or seperately. Therefore I look at beteasy.com website. It looks like I should print seperately for each data feed but in one go.

I think just need to clone then open by visual studio 2017 and hit run button.

I am very appreciated if you guys could give me some feedbacks even if I won't go through next round.

Thanks
Vo Nguyen

