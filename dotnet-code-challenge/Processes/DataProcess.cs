﻿using dotnet_code_challenge.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Processes
{
    public interface IDataProcess
    {
        void Process();
    }

    public class DataProcess : IDataProcess
    {
        private IFileService fileService;
        private IMappingService mappingService;
        private IPrintOutService printOutService;
        public DataProcess(IFileService fileService, IMappingService mappingService, IPrintOutService printOutService)
        {
            this.fileService = fileService;
            this.mappingService = mappingService;
            this.printOutService = printOutService;
        }
        public void Process()
        {
            printOutService.PrintOut(mappingService.MappingToModel(fileService.ReadFile()));
        }
    }
}