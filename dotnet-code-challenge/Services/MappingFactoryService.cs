﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Services
{
    public interface IMappingFactoryService
    {
        IMappingService GetService();
    }

    public class MappingFactoryService : IMappingFactoryService
    {
        private string fileName;
        public MappingFactoryService(string fileName)
        {
            this.fileName = fileName;
        }
        public IMappingService GetService()
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                if (fileName.ToLower().Contains(".xml"))
                {
                    return new XmlMappingService();
                }
                else
                if (fileName.ToLower().Contains(".json"))
                {
                    return new JsonMappingService();
                }
            }
            //TODO:log error
            return null;
        }
    }
}
