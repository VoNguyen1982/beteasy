﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace dotnet_code_challenge.Services
{
    public interface IFileService
    {
        string ReadFile();
    }

    public class FileService : IFileService
    {
        private string fileName { set; get; }
        public FileService(string fileName)
        {
            this.fileName = fileName;
        }
        public string ReadFile()
        {
            try
            {
                return File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {
                //TODO: log exception
            }
            return string.Empty;
        }
    }
}
