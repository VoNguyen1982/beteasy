﻿using dotnet_code_challenge.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace dotnet_code_challenge.Services
{
    public interface IMappingService
    {
        IViewModel MappingToModel(string data);
    }

    public class JsonMappingService : IMappingService
    {
        public IViewModel MappingToModel(string data)
        {
            var model = new ViewModel() { Rows = new List<Row>() };
            try
            {

                // convert json string to obj
                var obj = JsonConvert.DeserializeObject<WolferhamptonRace>(data);

                //horse name and price are in different places so I assumed that we need to extract them.
                //This loop will extra data and put into view model
                //Assume that the list of horses is in obj.RawData.Participants
                //And price is obj.RawData.Markets.Selections.Price
                //I consider key is Name in this case

                //TODO: it may have other ways like Lambda Expression or LINQ to do it better but time is running.

                foreach (var participant in obj.RawData.Participants)
                {
                    var row = new Row() { Name = participant.Name };

                    foreach (var market in obj.RawData.Markets)
                    {
                        foreach (var selection in market.Selections)
                        {
                            if (row.Name.Equals(selection.Tags.name))
                            {
                                row.Price = selection.Price;
                                break;
                            }
                        }
                        if (row.Price != 0) break;
                    }
                    model.Rows.Add(row);
                }
                return model;
            }
            catch (Exception ex)
            {
                //log error
            }
            return new ViewModel();
        }
    }

    public class XmlMappingService : IMappingService
    {
        public IViewModel MappingToModel(string data)
        {
            MemoryStream mStrm = new MemoryStream(Encoding.UTF8.GetBytes(data));
            CaulfieldRace obj = new CaulfieldRace();
            var model = new ViewModel() { Rows = new List<Row>() };

            try
            {
                // convert xml string to object.
                XmlSerializer serializer = new XmlSerializer(typeof(CaulfieldRace));
                obj = (CaulfieldRace)serializer.Deserialize(mStrm);

                //same here I consider obj.Races.Race.Horses.Horse.Number is a key to link to obj.Races.Race.Prices.Price.Horses.Horse._Number
                
                foreach (var horseName in obj.Races.Race.Horses.Horse)
                {
                    foreach (var horsePrice in obj.Races.Race.Prices.Price.Horses.Horse)
                    {
                        if (horseName.Number == horsePrice._Number)
                        {
                            horseName.Price = horsePrice.Price;
                            model.Rows.Add(new Row() { Name = horseName.Name, Price = horsePrice.Price });
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //log error
            }
            finally
            {
                mStrm.Dispose();
            }
            return model;
        }
    }
}
