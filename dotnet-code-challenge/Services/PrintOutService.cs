﻿using dotnet_code_challenge.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Services
{
    public interface IPrintOutService
    {
        void PrintOut(IViewModel obj);
    }
    public class PrintOutService : IPrintOutService
    {
        public void PrintOut(IViewModel obj) 
        {
            try
            {
                foreach (var horse in obj.OrderByAsc())
                {
                    Console.WriteLine(string.Format("{0} : {1}", horse.Name, horse.Price));
                }
            }
            catch (Exception ex)
            {
                //TODO: Log error
            }
        }
    }
}
