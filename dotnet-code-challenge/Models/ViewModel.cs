﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Models
{
    public interface IViewModel
    {
        List<Row> Rows { set; get; }
        List<Row> OrderByAsc();
    }
    public class ViewModel : IViewModel
    {
        public List<Row> Rows { set; get; }
        public List<Row> OrderByAsc()
        {
            Rows.Sort(delegate (Row x, Row y) { return x.Price.CompareTo(y.Price); });
            return Rows;
        }
    }
    public class Row
    {
        public string Name { set; get; }
        public decimal Price { set; get; }
    }
}
