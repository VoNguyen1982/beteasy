﻿using dotnet_code_challenge.Processes;
using dotnet_code_challenge.Services;
using System;
using System.IO;

namespace dotnet_code_challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string wFile = string.Format("{0}\\{1}", directory, "FeedData\\Wolferhampton_Race1.json");
                string cFile = string.Format("{0}\\{1}", directory, "FeedData\\Caulfield_Race1.xml");

                Console.WriteLine("--Wolferhampton Race");

                IMappingFactoryService mappingFactoryServiceForWolferhampton = new MappingFactoryService(wFile);
                IDataProcess wDataProcess = new DataProcess(new FileService(wFile), mappingFactoryServiceForWolferhampton.GetService(), new PrintOutService());
                wDataProcess.Process();

                Console.WriteLine("--Caulfield Race");
                IMappingFactoryService mappingFactoryServiceForCaulfield = new MappingFactoryService(cFile);
                IDataProcess cDataProcess = new DataProcess(new FileService(cFile), mappingFactoryServiceForCaulfield.GetService(), new PrintOutService());
                cDataProcess.Process();
            }
            catch (Exception ex)
            {
                //TODO: log error
            }
            Console.ReadKey();
        }
    }
}
